﻿// See https://aka.ms/new-console-template for more information
using Shared;

Console.WriteLine("Mi primera App!");
do
{
    try
    {
        Console.WriteLine("Nueva Persona: ");
        Persona p = new Persona();
        Console.Write("Nombre: ");
        p.Nombre = Console.ReadLine();
        Console.Write("Documento: ");
        p.Documento = Console.ReadLine();

        Console.WriteLine("-- Persona agregada --");
        Console.WriteLine("     Documento: " + p.Documento);
        Console.WriteLine("     Nombre: " + p.Nombre);
    }
    catch (Exception e)
    {
        Console.WriteLine(e.ToString());
    }

}while (!Console.ReadLine().Equals("EXIT"));


/*
int[] aux = { 2, 3, 6, 1, 0, 8, 9, 4, 5, 56, 10, 88, 23, 44, 11 };
aux.ToList().ForEach(x => Console.WriteLine(x+ "|"));
Console.WriteLine();

int[] aux2 = aux.ToList().OrderBy(x => x).ToArray();
aux2.ToList().ForEach(x => Console.WriteLine(x+ "|"));
Console.WriteLine();

int[] aux3 = aux.ToList().Where(x => x < 0).OrderBy(x => x).ToArray();
aux3.ToList().ForEach(x => Console.WriteLine(x+ "|"));
Console.WriteLine();
*/