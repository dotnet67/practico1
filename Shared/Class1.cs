﻿namespace Shared
{
    public class Persona
    {
        public string Nombre { get; set; } = "-- Sin nombre --";

        private string documento = "";
        public string Documento
        {
            get { return documento; }
            set
            {
                if (value.Length < 7)
                {
                    throw new Exception("El documento no es valido");
                }
                else
                {
                    documento = value.ToUpper();
                }
            }
        }
    }
}